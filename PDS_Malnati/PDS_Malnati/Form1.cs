﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDS_Malnati
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Deactivate += Form1_Deactivate;
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            this.Hide();
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.ActiveControl = null;
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                this.Hide();
                this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
                this.ActiveControl = null;
            }
            else
            {
                this.Show();
                this.WindowState = System.Windows.Forms.FormWindowState.Normal;
                this.Activate();
                this.Left = Cursor.Position.X - (this.Width / 2);
                this.Top = Screen.PrimaryScreen.WorkingArea.Bottom - this.Height;
            }

        }
    }
}
