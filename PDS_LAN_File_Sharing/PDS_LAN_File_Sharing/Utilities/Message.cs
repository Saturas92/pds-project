﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDS_LAN_File_Sharing.Utilities
{

    class Message
    {
        public enum MessageType : byte
        {
            FindHostsRequest = 1, //(Msg: Type)
            FindHostsResponse = 2, //(Msg: Type, Username, Data = User Image)
            FileTransferRequest = 3, //(Msg: Type, Username = Sender Username, Name = File Name, Size = File Size)
            FolderTransferRequest = 4, //(Msg: Type, Username = Sender Username, Name = Folder Name, Size = Total Folder Size)
            TransferAgreed = 5, //(Msg: Type)
            TransferRefused = 6, //(Msg: Type)
            FileHeader = 7, //A message of this type is followed by file data. (Msg: Type, Name = File Relative Path, Size = File Size)
            FolderHeader = 8, //A message of this type is a request to create a folder, it's not followed by any data. (Msg: Type, Name = Folder Relative Path)
            FolderTransferEnd = 9 //It signals that the folder trasfer is complete. (Msg: Type)
        }


        public MessageType Type { get; set; }
        public String Username { get; set; }
        public String Name { get; set; }
        public byte[] Data { get; set; }
        public long Size { get; set; }

        public Message()
        {

        }

        public Message(MessageType Type)
        {
            this.Type = Type;
        }

        public Message(MessageType Type, string Username, byte[] Data)
        {
            this.Type = Type;
            this.Username = Username;
            this.Data = Data;
        }

        public Message(MessageType Type, string Name)
        {
            this.Type = Type;
            this.Name = Name;
        }

        public Message(MessageType Type, string Name, long Size)
        {
            this.Type = Type;
            this.Name = Name;
            this.Size = Size;
        }

        public Message(MessageType Type, string Username, string Name, long Size)
        {
            this.Type = Type;
            this.Username = Username;
            this.Name = Name;
            this.Size = Size;
        }


        public string Serialize()
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
            serializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
            String messageJSON = JsonConvert.SerializeObject(this, serializerSettings);
            StringBuilder sb = new StringBuilder(messageJSON);
            sb.Append("<EOF>");
            if(Type != MessageType.FindHostsResponse)
            {
                if(sb.Length > 1024)
                {
                    throw new Exception("Serializing error: Message too long.");
                }
            }
            return sb.ToString();
        }
    }
}
