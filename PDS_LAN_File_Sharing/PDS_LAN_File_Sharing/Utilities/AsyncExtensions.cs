﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PDS_LAN_File_Sharing.Utilities
{
    public static class AsyncExtensions
    {
        public static async Task<T> WithCancellation<T>(this Task<T> task, CancellationToken cancellationToken, int timeout = 30000)
        {
            var tcs = new TaskCompletionSource<bool>();
            if (timeout == Timeout.Infinite)
            {
                using (cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
                {
                    if (task != await Task.WhenAny(task, tcs.Task))
                    {
                        throw new OperationCanceledException(cancellationToken);
                    }
                }
            }
            else
            {
                var timeoutTask = Task.Delay(timeout);
                using (cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
                {
                    if (task != await Task.WhenAny(task, tcs.Task, timeoutTask))
                    {
                        if (tcs.Task.IsCompleted)
                        {
                            throw new OperationCanceledException(cancellationToken);
                        }
                        else if (timeoutTask.IsCompleted)
                        {
                            throw new Exception("Timeout exceeded");
                        }
                    }
                }
            }
            return task.Result;
        }

        public static async Task WithCancellation(this Task task, CancellationToken cancellationToken, int timeout = 60000)
        {
            var tcs = new TaskCompletionSource<bool>();
            if (timeout == Timeout.Infinite)
            {
                using (cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
                {
                    if (task != await Task.WhenAny(task, tcs.Task))
                    {
                        throw new OperationCanceledException(cancellationToken);
                    }
                }
            }
            else
            {
                var timeoutTask = Task.Delay(timeout);
                using (cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
                {
                    if (task != await Task.WhenAny(task, tcs.Task, timeoutTask))
                    {
                        if (tcs.Task.IsCompleted)
                        {
                            throw new OperationCanceledException(cancellationToken);
                        }
                        else if (timeoutTask.IsCompleted)
                        {
                            throw new Exception("Timeout exceeded");
                        }
                    }
                }
            }
            return;
        }

        public static async Task<T> WithCancellation<T>(this Task<T> task, int timeout = 60000)
        {
            var timeoutTask = Task.Delay(timeout);
            if (task != await Task.WhenAny(task, timeoutTask))
            {
                throw new Exception("Timeout exceeded");
            }
            return task.Result;
        }

        public static async Task WithCancellation(this Task task,int timeout = 60000)
        {
            var timeoutTask = Task.Delay(timeout);
            if (task != await Task.WhenAny(task, timeoutTask))
            {
                throw new Exception("Timeout exceeded");
            }
            return;
        }


        /// <summary>
        ///     Makes an asynchronous request for a remote host connection
        /// </summary>
        /// <param name="remoteEp">
        ///     A System.Net.EndPoint that represents the remote host
        /// </param>
        /// <returns>Task</returns>
        public static Task ConnectTask(this Socket s, EndPoint remoteEp)
        {
            return Task.Factory.FromAsync(
                s.BeginConnect(remoteEp, null, null),
                s.EndConnect
            );
        }

        /// <summary>
        ///     Asynchronously waits to accept an incoming connection.
        /// </summary>
        /// <returns>Task&lt;Socket&gt;</returns>
        public static Task<Socket> AcceptTask(this Socket s)
        {
            return Task.Factory.FromAsync<Socket>(
                s.BeginAccept(null, null),
                s.EndAccept
            );
        }

        /// <summary>
        ///     Asynchronously receives data from a connected System.Net.Sockets.Socket
        /// </summary>
        /// <param name="buffer">
        ///     An array of type System.Byte that is the storage location for the received data
        /// </param>
        /// <param name="offset">
        ///     The zero-based position in the buffer parameter at which to store the received data
        /// </param>
        /// <param name="size">The number of bytes to receive</param>
        /// <param name="flags">A bitwise combination of the System.Net.Sockets.SocketFlags values</param>
        /// <returns>Task&lt;int&gt;</returns>
        public static Task<int> ReceiveTask(this Socket s, byte[] buffer, int offset, int size, SocketFlags flags)
        {
            return Task.Factory.FromAsync<int>(
                s.BeginReceive(buffer, offset, size, flags, null, null),
                s.EndReceive
            );
        }

        /// <summary>
        ///     Sends data asynchronously to a connected System.Net.Sockets.Socket
        /// </summary>
        /// <param name="buffer">
        ///     An array of type System.Byte that contains the data to send
        /// </param>
        /// <param name="offset">
        ///     The zero-based position in the buffer parameter at which to begin sending data
        /// </param>
        /// <param name="size">The number of bytes to send</param>
        /// <param name="flags">A bitwise combination of the System.Net.Sockets.SocketFlags values</param>
        /// <returns>Task&lt;int&gt;</returns>
        public static Task<int> SendTask(this Socket s, byte[] buffer, int offset, int size, SocketFlags flags)
        {
            return Task.Factory.FromAsync<int>(
                s.BeginSend(buffer, offset, size, flags, null, null),
                s.EndSend
            );
        }

        /// <summary>
        ///     Asynchronously requests to disconnect from a remote endpoint.
        /// </summary>
        /// <param name="reuseSocket">Re-use the socket after connection is closed</param>
        /// <returns>Task</returns>
        public static Task DisconnectTask(this Socket s, bool reuseSocket)
        {
            return Task.Factory.FromAsync(
                s.BeginDisconnect(reuseSocket, null, null),
                s.EndDisconnect
            );
        }


    }
}
