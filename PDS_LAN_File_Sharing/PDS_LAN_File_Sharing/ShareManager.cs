﻿using Newtonsoft.Json;
using PDS_LAN_File_Sharing.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace PDS_LAN_File_Sharing
{
    public class ShareManager
    {
        public ObservableCollection<Host> hosts;
        public ShareWindow shareWindow;

        Task receiverFindHostsTask;
        Task receiverSocketListenerTask;
        CancellationTokenSource cancellationTokenSource;
        private string dataPath;

        public ShareManager()
        {
            receiverFindHostsTask = Task.Run(() => ReceiverFindHosts()); //Put ReceiverFindHosts on a separate thread
            receiverSocketListenerTask = Task.Run(() => ReceiverSocketListener()); //Put ReceiverSocketListener on a separate thread

            hosts = new ObservableCollection<Host>();
        }


        internal void CreateShareWindow(string dataPath)
        {
            this.dataPath = dataPath;
            cancellationTokenSource = new CancellationTokenSource();
            var senderFindHostsTask = SenderFindHostsAsync(cancellationTokenSource.Token);
            
            shareWindow = new ShareWindow(this);
            shareWindow.Title = "Share \"" + Path.GetFileName(dataPath) + "\" with...";
            shareWindow.Show();
            shareWindow.Focus();
            shareWindow.Activate();
            shareWindow.Closing += new CancelEventHandler(ShareWindow_Closing);

            //shareWindow.HostsListBox.DataContext = this;
            shareWindow.HostsListBox.ItemsSource = hosts;

        }

        private void ShareWindow_Closing(object sender, CancelEventArgs e)
        {
            cancellationTokenSource.Cancel();
            hosts.Clear();
            dataPath = null;
        }

        public async Task ShareWithSelectedHosts(IList selectedHosts)
        {
            if (!Directory.Exists(dataPath) && !File.Exists(dataPath))
            {
                await Application.Current.Dispatcher.InvokeAsync(() =>
                {
                    MessageBox.Show("\"" + Path.GetFileName(dataPath) + "\" doesn't exist anymore.", "LAN File Sharing - Error", MessageBoxButton.OK, MessageBoxImage.Error);
                });
                shareWindow.Close();
                return;
            }
            string tempDataPath = dataPath;
            foreach (Host host in selectedHosts.Cast<Host>())
            {
                TransferManager transferManager = new TransferManager(tempDataPath, host);
            }

        }


        private async Task SenderFindHostsAsync(CancellationToken cancellationToken)
        {
            string multicastAddressString = "224.0.0.3";
            int remotePortNumber = 2222;
            UdpClient udpClient = new UdpClient();

            IPAddress multicastAddress = IPAddress.Parse(multicastAddressString);
            try
            {
                udpClient.JoinMulticastGroup(multicastAddress);
                IPEndPoint remoteEndPoint = new IPEndPoint(multicastAddress, remotePortNumber);

                //Create and send a FindHostRequest Message
                Message senderRequestMessage = new Message(Message.MessageType.FindHostsRequest);
                String senderRequestString = senderRequestMessage.Serialize();

                byte[] requestData = Encoding.ASCII.GetBytes(senderRequestString);
                cancellationToken.ThrowIfCancellationRequested();
                await udpClient.SendAsync(requestData, requestData.Length, remoteEndPoint).WithCancellation<int>(cancellationToken);
            }
            catch (OperationCanceledException)
            {
                udpClient.Close();
                return;
            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Error while setting UPD socket up", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error while setting UPD socket up:\n" + e.ToString());
                udpClient.Close();
                shareWindow.Close();
                return;
            }

            //Wait for a FindHostsResponse Message 
            while (true)
            {
                UdpReceiveResult responseData;
                try
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    responseData = await udpClient.ReceiveAsync().WithCancellation<UdpReceiveResult>(cancellationToken, Timeout.Infinite);
                    String responseString = Encoding.ASCII.GetString(responseData.Buffer);

                    Console.WriteLine("Message Received: " + responseString);

                    Message responseMessage = JsonConvert.DeserializeObject<Message>(responseString, new JsonSerializerSettings { CheckAdditionalContent = false });

                    if (responseMessage.Type == Message.MessageType.FindHostsResponse)
                    {
                        Bitmap userImage;
                        using (var ms = new MemoryStream(responseMessage.Data))
                        {
                            userImage = new Bitmap(ms);
                        }
                        BitmapSource userImageBitmapSource = Imaging.CreateBitmapSourceFromHBitmap(userImage.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

                        Host host = new Host(responseMessage.Username, userImageBitmapSource, responseData.RemoteEndPoint);
                        hosts.Add(host);
                    }
                    else
                    {
                        throw new Exception("SenderReceiveFindHostsResponse - Message with unknown message type, expected FindHostsResponse.");
                    }

                }
                catch (OperationCanceledException)
                {
                    udpClient.Close();
                    return;
                }
                catch (Exception e)
                {
                    App.notifyIcon.ShowBalloonTip("Error while receiving a UPD FindHostsResponse message", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                    Console.WriteLine("Error while receiving a UPD FindHostsResponse message:\n" + e.ToString());
                }

            }

        }

        private void ReceiverFindHosts()
        {
            string multicastAddressString = "224.0.0.3";
            int localPortNumber = 2222;

            UdpClient udpClient = new UdpClient();

            try
            {
                //EP of multicast address I receive request from
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, localPortNumber);
                udpClient.Client.Bind(localEndPoint);

                IPAddress multicastaddress = IPAddress.Parse(multicastAddressString);
                udpClient.JoinMulticastGroup(multicastaddress);
            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Error in UPD client binding", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error in UPD client binding:\n" + e.ToString());
                Application.Current.Shutdown();
            }

            //Convert User Image to byte[]
            System.Drawing.ImageConverter _imageConverter = new System.Drawing.ImageConverter();
            byte[] responseImage = (byte[])_imageConverter.ConvertTo(Manager.UserImage, typeof(byte[]));

            //Create a json message (FindHostsResponse), with username and user image, converting it then to byte[]
            Message receiverResponseMessage = new Message(Message.MessageType.FindHostsResponse, Manager.UserName, responseImage);
            String receiverResponseString = receiverResponseMessage.Serialize();

            byte[] responseData = Encoding.ASCII.GetBytes(receiverResponseString);

            //Initialize remoteEndPoint (it will be filled with the receive function)
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

            //Get local ip address
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0]; 
            foreach (var ip in ipHostInfo.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = ip;
                    break;
                }
            }
            Console.WriteLine("Local Username: {0}\nLocal IP: {1}", Manager.UserName, ipAddress);


            //Wait for a FindHostsRequest Message
            while (true)
            {
                try
                {
                    Byte[] requestData = udpClient.Receive(ref remoteEndPoint);
                    if (Manager.StatePublic == true /*&& !remoteEndPoint.Address.Equals(ipAddress)*/)
                    {
                        String requestString = Encoding.ASCII.GetString(requestData);
                        Console.WriteLine("Message Received: " + requestString);
                        Message requestMessage = JsonConvert.DeserializeObject<Message>(requestString, new JsonSerializerSettings { CheckAdditionalContent = false });
                        //If the message received is a FindHostsRequest Message, respond
                        if (requestMessage.Type == Message.MessageType.FindHostsRequest)
                        {
                            udpClient.Send(responseData, responseData.Length, remoteEndPoint);
                        }
                        else
                        {
                            throw new Exception("ReceiverFindHosts -  Message with unknown message type, expected FindHostsRequest.");
                        }
                    }

                }
                catch (Exception e)
                {
                    App.notifyIcon.ShowBalloonTip("Error while receiving a UDP FindHostRequest Message", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                    Console.WriteLine("Error while receiving a UDP FindHostRequest Message:\n" + e.ToString());
                }
            }

        }

        private async Task ReceiverSocketListener()
        {
            // Establish the local endpoint for the socket.
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0]; // Default value
            foreach (var ip in ipHostInfo.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = ip;
                    break;
                }
            }
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 2222);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);
            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Socket Listener Binding Error", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Socket Listener Binding Error:\n" + e.ToString());
                Application.Current.Shutdown();
            }
            while (true)
            {
                Socket hostSocket = null;
                try
                {
                    // Start an asynchronous socket to listen for connections.
                    hostSocket = await listener.AcceptTask();
                    var task = ReceiverSocketTransferAsync(hostSocket);
                }
                catch (Exception e)
                {
                    App.notifyIcon.ShowBalloonTip("Socket Listener Error", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                    Console.WriteLine("Socket Listener Error:\n" + e.ToString());
                    if(hostSocket != null)
                    {
                        hostSocket.Shutdown(SocketShutdown.Both);
                        hostSocket.Close();
                    }
                }
            }


        }

        private async Task ReceiverSocketTransferAsync(Socket hostSocket)
        {
            try
            {
                //Receive first message (expected FileTransferRequest or FolderTransferRequest, max 1024 bytes message)
                byte[] dataBuffer = new Byte[1024];
                int bytesReceived = await hostSocket.ReceiveTask(dataBuffer, 0, dataBuffer.Length, SocketFlags.None).WithCancellation();
                string requestString = Encoding.ASCII.GetString(dataBuffer, 0, bytesReceived);
                if (bytesReceived == 0 || requestString.IndexOf("<EOF>") == -1)
                {
                    throw new Exception("ReceiverSocketTransfer - Error while receiving transfer request message (FileTransferRequest or FolderTransferRequest).");
                }
                Console.WriteLine("Message Received: " + requestString);

                string destinationFolder = Manager.DestinationFolderPath;

                //Deserialize first message
                Message requestMessage = JsonConvert.DeserializeObject<Message>(requestString, new JsonSerializerSettings { CheckAdditionalContent = false });

                //Check if the message received is a FileTransferRequest Message or a FolderTransferRequest
                if (requestMessage.Type == Message.MessageType.FileTransferRequest)
                {
                    //FileTransferRequest Handling
                    if (Manager.AutomaticAccept == false)
                    {
                        //If automaticAccept is false, create MessageBox asking user confirmation to proceed
                        MessageBoxResult result = await Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            string messageBoxString = requestMessage.Username + " wants to share with you \"" + requestMessage.Name + "\" (" + Utils.SizeSuffix(requestMessage.Size) + "). Do you want to proceed with the trasfer?";
                            return MessageBox.Show(messageBoxString, "LAN File Sharing - File Transfer Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        });

                        if (result == MessageBoxResult.No)
                        {
                            //Create and send a TrasferRefused Message
                            Message receiverTranferRefuse = new Message(Message.MessageType.TransferRefused);
                            string receiverTranferRefuseJSON = receiverTranferRefuse.Serialize();
                            byte[] refuseData = Encoding.ASCII.GetBytes(receiverTranferRefuseJSON);

                            await hostSocket.SendTask(refuseData, 0, refuseData.Length, SocketFlags.None).WithCancellation();
                            hostSocket.Shutdown(SocketShutdown.Both);
                            hostSocket.Close();
                            return;
                        }

                    }

                    //Create and send a TransferAgreed Message
                    Message receiverTranferAgreed = new Message(Message.MessageType.TransferAgreed);
                    string receiverTranferAgreedString = receiverTranferAgreed.Serialize();
                    byte[] agreedData = Encoding.ASCII.GetBytes(receiverTranferAgreedString);

                    await hostSocket.SendTask(agreedData, 0, agreedData.Length, SocketFlags.None).WithCancellation();

                    //Wait for File Trasfer
                    await ReceiverSocketSingleDataTransfer(hostSocket, destinationFolder, requestMessage.Username);

                    //Show BaloonTip from tray icon
                    App.notifyIcon.ShowBalloonTip("\"" + requestMessage.Name + "\"", "File transfer from " + requestMessage.Username + " completed (" + Utils.SizeSuffix(requestMessage.Size) + ").", Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Info);

                    //Close socket
                    hostSocket.Shutdown(SocketShutdown.Both);
                    hostSocket.Close();

                }
                else if (requestMessage.Type == Message.MessageType.FolderTransferRequest)
                {
                    //FolderTransferRequest Handling
                    if (Manager.AutomaticAccept == false)
                    {
                        //If automaticAccept is false, create MessageBox asking user confirmation to proceed
                        MessageBoxResult result = await Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            string messageBoxString = requestMessage.Username + " wants to share with you \"" + requestMessage.Name + "\" (" + Utils.SizeSuffix(requestMessage.Size) + "). Do you want to proceed with the trasfer?";
                            return MessageBox.Show(messageBoxString, "LAN File Sharing - Folder Transfer Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        });

                        if (result == MessageBoxResult.No)
                        {
                            //Create and send a TrasferRefused Message
                            Message receiverTranferRefuse = new Message(Message.MessageType.TransferRefused);
                            string receiverTranferRefuseJSON = receiverTranferRefuse.Serialize();
                            byte[] refuseData = Encoding.ASCII.GetBytes(receiverTranferRefuseJSON);

                            await hostSocket.SendTask(refuseData, 0, refuseData.Length, SocketFlags.None).WithCancellation();
                            hostSocket.Shutdown(SocketShutdown.Both);
                            hostSocket.Close();
                            return;
                        }

                    }

                    //Create and send a TransferAgreed Message
                    Message receiverTranferAgreed = new Message(Message.MessageType.TransferAgreed);
                    string receiverTranferAgreedString = receiverTranferAgreed.Serialize();
                    byte[] agreedData = Encoding.ASCII.GetBytes(receiverTranferAgreedString);
                    await hostSocket.SendTask(agreedData, 0, agreedData.Length, SocketFlags.None).WithCancellation();

                    //Create initial folder
                    Directory.CreateDirectory(Path.GetDirectoryName(destinationFolder + requestMessage.Name));

                    //Wait for File Transfer. 
                    //ReceiverSocketSingleDataTransfer returns true if there are more files to receive, false when folder transfer is completed 
                    while (await ReceiverSocketSingleDataTransfer(hostSocket, destinationFolder, requestMessage.Username)) ;

                    //Show BaloonTip from tray icon
                    App.notifyIcon.ShowBalloonTip("\"" + requestMessage.Name + "\"", "Folder transfer from " + requestMessage.Username + " completed (" + Utils.SizeSuffix(requestMessage.Size) + ").", Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Info);

                    //Close socket
                    hostSocket.Shutdown(SocketShutdown.Both);
                    hostSocket.Close();

                }
                else
                {
                    throw new Exception("ReceiverSocketTransfer - Message with an unknown message type, expected FileTransferRequest or FolderTransferRequest.");
                }
            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Socket Listener Error", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Socket Listener Error:\n" + e.ToString());
                if (hostSocket != null)
                {
                    hostSocket.Shutdown(SocketShutdown.Both);
                    hostSocket.Close();
                }
            }
        }

        //ReceiverSocketSingleDataTransfer returns true if there are more files to receive, false when folder transfer is completed 
        private async Task<bool> ReceiverSocketSingleDataTransfer(Socket hostSocket, string destinationFolder, string senderUsername)
        {
            //Wait for header message (this type of message has a standard size of 1024 bytes)
            byte[] dataBuffer = new Byte[1024];
            int bytesReceived = await hostSocket.ReceiveTask(dataBuffer, 0, dataBuffer.Length, SocketFlags.None).WithCancellation();
            string headerString = Encoding.ASCII.GetString(dataBuffer, 0, bytesReceived);
            if (bytesReceived == 0 || headerString.IndexOf("<EOF>") == -1)
            {
                throw new Exception("ReceiverSocketSingleDataTransfer - Error while receiving header message from " + senderUsername + " (FileHeader, FolderHeader or FolderTransferEnd).");
            }
            Console.WriteLine("Message Received: " + headerString);

            Message headerMessage = JsonConvert.DeserializeObject<Message>(headerString, new JsonSerializerSettings { CheckAdditionalContent = false });

            //Check if the message received is a FileHeader, a FolderHeader or a FolderTransferEnd message
            if (headerMessage.Type == Message.MessageType.FileHeader)
            {
                string completePath = destinationFolder + headerMessage.Name;
                Directory.CreateDirectory(Path.GetDirectoryName(completePath));

                // Manage file with same name in same directory
                if (File.Exists(completePath))
                {
                    int i = 1;
                    String newPath = Path.GetDirectoryName(completePath) + "\\" + Path.GetFileNameWithoutExtension(completePath) + " (" + i + ")" + Path.GetExtension(completePath);

                    while (File.Exists(newPath))
                    {
                        i++;
                        newPath = Path.GetDirectoryName(completePath) + "\\" + Path.GetFileNameWithoutExtension(completePath) + " (" + i + ")" + Path.GetExtension(completePath);
                    }
                    completePath = newPath;
                }

                //Get File Data
                dataBuffer = new Byte[4096];
                using (FileStream fileStream = File.Create(completePath))
                {
                    try
                    {
                        long bytesLeftToReceive = headerMessage.Size;
                        while (bytesLeftToReceive > 0)
                        {
                            //Receive a chunk
                            if (bytesLeftToReceive >= dataBuffer.Length)
                            {
                                bytesReceived = await hostSocket.ReceiveTask(dataBuffer, 0, dataBuffer.Length, SocketFlags.None).WithCancellation();
                                if (bytesReceived == 0)
                                {
                                    throw new Exception("ReceiverSocketSingleDataTransfer - Error while receiving file data from " + senderUsername + ".");
                                }
                            }
                            else
                            {
                                bytesReceived = await hostSocket.ReceiveTask(dataBuffer, 0, (int)bytesLeftToReceive, SocketFlags.None).WithCancellation();
                                if (bytesReceived == 0)
                                {
                                    throw new Exception("ReceiverSocketSingleDataTransfer - Error while receiving file data from " + senderUsername + ".");
                                }
                            }

                            // write to file
                            await fileStream.WriteAsync(dataBuffer, 0, bytesReceived);

                            //update bytesLeftToReceive
                            bytesLeftToReceive -= bytesReceived;
                        }
                        Console.WriteLine("Received \"{0}\" from {1} ({2}).", headerMessage.Name, senderUsername, hostSocket.RemoteEndPoint);
                        fileStream.Close();
                    }
                    catch (Exception e)
                    {
                        fileStream.Close();
                        File.Delete(completePath);
                        throw e;
                    }
                }

            }
            else if (headerMessage.Type == Message.MessageType.FolderHeader)
            {
                //Create folder
                string completePath = destinationFolder + headerMessage.Name;
                Directory.CreateDirectory(completePath);
            }
            else if (headerMessage.Type == Message.MessageType.FolderTransferEnd)
            {
                //Folder transfer is completed
                return false;
            }
            else
            {
                throw new Exception("ReceiverSocketSingleFileTransfer - Message received from " + senderUsername + " with an unknown message type, expected FileHeader, FolderHeader or FolderTransferEnd.");
            }

            return true;

        }


    }
}
