﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// Logica di interazione per TransferWindow.xaml
    /// </summary>
    public partial class TransferWindow : Window, INotifyPropertyChanged
    {
        private string _progressString;
        public string ProgressString
        {
            get
            {
                return _progressString;
            }
            set
            {
                _progressString = value;
                OnPropertyChanged("ProgressString");
            }
        }

        private string _remainingTimeString;
        public string RemainingTimeString
        {
            get
            {
                return _remainingTimeString;
            }
            set
            {
                _remainingTimeString = value;
                OnPropertyChanged("RemainingTimeString");
            }
        }

        private string _currentOperationString;
        public string CurrentOperationString
        {
            get
            {
                return _currentOperationString;
            }
            set
            {
                _currentOperationString = value;
                OnPropertyChanged("CurrentOperationString");
            }
        }


        public TransferWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
