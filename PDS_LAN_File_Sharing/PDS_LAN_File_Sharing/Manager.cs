﻿using Microsoft.Win32;
using PDS_LAN_File_Sharing.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// App main class
    /// </summary>
    class Manager
    {
        public static Bitmap UserImage { get; set; }
        public static string UserName { get; set; }

        public static bool StatePublic { get; set; }
        public static string DestinationFolderPath { get; set; }
        public static bool AutomaticAccept { get; set; }

        public static ShareManager shareManager;

        public Manager()
        {
            GetUserInfo();

            //Settings initialization
            StatePublic = true;
            DestinationFolderPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString() + "\\ReceivedData\\";
            Directory.CreateDirectory(DestinationFolderPath);
            AutomaticAccept = false;

            shareManager = new ShareManager();
        }

        /// <summary>
        /// Save the user's name and image.
        /// </summary>
        private void GetUserInfo()
        {
            string userImageName = null;
            string appDataPath = null;

            try
            {
                //Find Username and appDataPath
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Volatile Environment"))
                {
                    if (registryKey != null)
                    {
                        UserName = registryKey.GetValue("USERNAME").ToString();
                        appDataPath = registryKey.GetValue("APPDATA").ToString();
                    }

                }

                //Find name of current user's account picture 
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\AccountPicture"))
                {
                    if (registryKey != null)
                    {
                        Object value = registryKey.GetValue("SourceId");
                        if (value != null)
                        {
                            userImageName = value.ToString();
                        }
                    }
                }

            }
            catch (Exception ex) 
            {
                App.notifyIcon.ShowBalloonTip("Error reading Register key", ex.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error reading Register key:\n" + ex.ToString());
                Application.Current.Shutdown();
            }

            //If an account image name was found, save in UserImage the account image
            if (userImageName != null)
            {
                string imagePath = appDataPath + "\\Microsoft\\Windows\\AccountPictures\\" + userImageName + ".accountpicture-ms";
                UserImage = AccountPicConverter.ConvertAccountPic96(imagePath);

            }
            //If not, save in UserImage default image
            else
            {
                UserImage = PDS_LAN_File_Sharing.Properties.Resources.User;
            }

        }

        internal void ShareData(string dataPath)
        {
            if (shareManager.shareWindow != null)
                shareManager.shareWindow.Close();
            shareManager.CreateShareWindow(dataPath);
        }



    }
}
