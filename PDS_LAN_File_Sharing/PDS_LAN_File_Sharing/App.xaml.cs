﻿using Hardcodet.Wpf.TaskbarNotification;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using Microsoft.Shell;
using System.IO;
using System.Text;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application, ISingleInstanceApp
    {
        public static TaskbarIcon notifyIcon;
        private static Manager manager;

        private const string fileMenuName = "*\\shell\\LANFileSharing";
        private const string fileCommand = "*\\shell\\LANFileSharing\\command";
        private const string dirMenuName = "Directory\\shell\\LANFileSharing";
        private const string dirCommand = "Directory\\shell\\LANFileSharing\\command";

        private const string Unique = "LAN_File_Sharing";

        [STAThread]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                var application = new App();
                manager = new Manager();
                application.InitializeComponent();
                application.Run();
                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //create the notifyicon (resource declared in NotifyIconResourceDictionary.xaml)
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");

            // Add entry to context menu in Window Explorer
            AddAppRegistryKey();
        }

        #region ISingleInstanceApp Members
        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            if(args.Count > 1)
            {
                //if data path had spaces, restore correct data path
                StringBuilder dataPathBuilder = new StringBuilder();
                for(int i = 1; i < args.Count-1; i++)
                {
                    dataPathBuilder.Append(args[i]);
                    dataPathBuilder.Append(" ");
                }
                dataPathBuilder.Append(args[args.Count - 1]);

                manager.ShareData(dataPathBuilder.ToString());

            }
            return true;
        }
        #endregion

        private void AddAppRegistryKey()
        {
            RegistryKey fileRegMenu = null;
            RegistryKey fileRegCmd = null;
            RegistryKey dirRegMenu = null;
            RegistryKey dirRegCmd = null;
            String exePath = System.Reflection.Assembly.GetExecutingAssembly().Location + " %1";
            String iconPath = Path.Combine(Environment.CurrentDirectory, @"Resources\\AppIcon.ico");

            try
            {
                //Add file context menu registry key
                fileRegMenu = Registry.ClassesRoot.CreateSubKey(fileMenuName);
                if (fileRegMenu != null)
                {
                    fileRegMenu.SetValue("", "Share with LAN File Sharing");
                    fileRegMenu.SetValue("Icon", iconPath);
                }

                fileRegCmd = Registry.ClassesRoot.CreateSubKey(fileCommand);
                if (fileRegCmd != null)
                    fileRegCmd.SetValue("", exePath);

                //Add directory context menu registry key
                dirRegMenu = Registry.ClassesRoot.CreateSubKey(dirMenuName);
                if (dirRegMenu != null)
                {
                    dirRegMenu.SetValue("", "Share with LAN File Sharing");
                    dirRegMenu.SetValue("Icon", iconPath);
                }
                dirRegCmd = Registry.ClassesRoot.CreateSubKey(dirCommand);
                if (dirRegCmd != null)
                    dirRegCmd.SetValue("", exePath);
            }
            catch (Exception ex)
            {
                notifyIcon.ShowBalloonTip("Error adding Register key", ex.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error adding Register key:\n" + ex.ToString());
                Current.Shutdown();
            }
            finally
            {
                if (fileRegMenu != null)
                    fileRegMenu.Close();
                if (fileRegCmd != null)
                    fileRegCmd.Close();

                if (dirRegMenu != null)
                    dirRegMenu.Close();
                if (dirRegCmd != null)
                    dirRegCmd.Close();
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose();
            RemoveAppRegistryKey();
            base.OnExit(e);
        }

        private void RemoveAppRegistryKey()
        {
            try
            {
                RegistryKey reg = Registry.ClassesRoot.OpenSubKey(fileCommand);
                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(fileCommand);
                }
                reg = Registry.ClassesRoot.OpenSubKey(fileMenuName);
                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(fileMenuName);
                }

                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(dirCommand);
                }
                reg = Registry.ClassesRoot.OpenSubKey(dirMenuName);
                if (reg != null)
                {
                    reg.Close();
                    Registry.ClassesRoot.DeleteSubKey(dirMenuName);
                }
            }
            catch (Exception ex)
            {
                notifyIcon.ShowBalloonTip("Error removing Register key", ex.ToString(), Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error removing Register key:\n" + ex.ToString());
            }
        }

    }
}
