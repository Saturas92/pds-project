﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// Logica di interazione per ShareWindow.xaml
    /// </summary>
    public partial class ShareWindow : Window
    {
        private ShareManager shareManager;

        public ShareWindow(ShareManager shareManager)
        {
            InitializeComponent();
            this.shareManager = shareManager;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ShareButton_Click(object sender, RoutedEventArgs e)
        {
            shareManager.ShareWithSelectedHosts(this.HostsListBox.SelectedItems);
            this.Close();
        }
    }
}
