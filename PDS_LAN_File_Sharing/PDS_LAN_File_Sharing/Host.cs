﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PDS_LAN_File_Sharing
{
    public class Host
    {
        public String Name { get; set; }
        public BitmapSource Image { get; set; }
        public IPEndPoint EndPoint { get; set; }

        public Host(string name, BitmapSource image)
        {
            this.Name = name;
            this.Image = image;
        }

        public Host(string name, BitmapSource image, IPEndPoint endPoint)
        {
            this.Name = name;
            this.Image = image;
            this.EndPoint = endPoint;
        }

    }
}
