﻿using Microsoft.Win32;
using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// Logica di interazione per SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window, INotifyPropertyChanged
    {
        private string _destinationFolderPathTemp;
        public string DestinationFolderPathTemp
        {
            get
            {
                return _destinationFolderPathTemp;
            }
            set
            {
                _destinationFolderPathTemp = value;
                OnPropertyChanged("DestinationFolderPathTemp");
            }
        }

        private bool _automaticAcceptTemp;
        public bool AutomaticAcceptTemp
        {
            get
            {
                return _automaticAcceptTemp;
            }
            set
            {
                _automaticAcceptTemp = value;
                OnPropertyChanged("AutomaticAcceptTemp");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }


        public SettingsWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            DestinationFolderPathTemp = Manager.DestinationFolderPath;
            AutomaticAcceptTemp = Manager.AutomaticAccept;
        }

        private void BrowseFolder_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog folderBrowserDialog = new VistaFolderBrowserDialog();
            folderBrowserDialog.SelectedPath = DestinationFolderPathTemp;
            if (folderBrowserDialog.ShowDialog() == true)
            {
                DestinationFolderPathTemp = folderBrowserDialog.SelectedPath;
            }

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Manager.DestinationFolderPath = DestinationFolderPathTemp;
            Manager.AutomaticAccept = AutomaticAcceptTemp;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
