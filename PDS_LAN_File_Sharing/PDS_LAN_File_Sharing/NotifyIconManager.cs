﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PDS_LAN_File_Sharing
{
    /// <summary>
    /// Provides bindable properties and commands for the NotifyIcon. The view model is assigned to the NotifyIcon in XAML.
    /// </summary>
    class NotifyIconManager : INotifyPropertyChanged
    {
        private const string stateButtonTextPublic = "State:\nPublic";
        private const string stateButtonTextPrivate = "State:\nPrivate";
        private const string stateButtonColorPublic = "#FF1D8C9E";
        private const string stateButtonColorPrivate = "#FF303030";

        private string _stateButtonString;
        public string StateButtonString
        {
            get
            {
                return _stateButtonString;
            }
            set
            {
                _stateButtonString = value;
                OnPropertyChanged("StateButtonString");
            }
        }

        private string _stateButtonColor;
        public string StateButtonColor
        {
            get
            {
                return _stateButtonColor;
            }
            set
            {
                _stateButtonColor = value;
                OnPropertyChanged("StateButtonColor");
            }
        }

        public BitmapSource UserImage { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public NotifyIconManager()
        {
            UserImage = Imaging.CreateBitmapSourceFromHBitmap(Manager.UserImage.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            if (Manager.StatePublic == true)
            {
                StateButtonString = stateButtonTextPublic;
                StateButtonColor = stateButtonColorPublic;
            }
            else
            {
                StateButtonString = stateButtonTextPrivate;
                StateButtonColor = stateButtonColorPrivate;
            }
        }

        /// <summary>
        /// Change state of the application (Private/Public).
        /// </summary>
        public ICommand ChangeStateCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () =>
                    {
                        if (Manager.StatePublic == true)
                        {
                            StateButtonString = stateButtonTextPrivate;
                            StateButtonColor = stateButtonColorPrivate;
                            Manager.StatePublic = false;
                        }
                        else
                        {
                            StateButtonString = stateButtonTextPublic;
                            StateButtonColor = stateButtonColorPublic;
                            Manager.StatePublic = true;
                        }
                    }
                };
            }
        }

        /// <summary>
        /// Open Settings Window.
        /// </summary>
        public ICommand OpenSettingsCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () =>
                    {
                        SettingsWindow settingsWindow = new SettingsWindow();
                        settingsWindow.Show();
                        App.notifyIcon.TrayPopupResolved.IsOpen = false;
                    }
                };
            }
        }

        /// <summary>
        /// Shuts down the application.
        /// </summary>
        public ICommand ExitApplicationCommand
        {
            get
            {
                return new DelegateCommand { CommandAction = () => Application.Current.Shutdown() };
            }
        }

    }
}
