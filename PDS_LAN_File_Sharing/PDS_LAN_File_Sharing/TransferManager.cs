﻿using Newtonsoft.Json;
using PDS_LAN_File_Sharing.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PDS_LAN_File_Sharing
{
    class TransferManager
    {
        public TransferWindow transferWindow;
        public Host Host { get; set; }
        public string DataPath { get; set; }
        private long totalDataSent;
        private long totalDataSize;
        private DateTime startTime;
        private CancellationTokenSource cancellationTokenSource;
        private List<DataInfo> dataList;
        
        private class DataInfo
        {
            public enum DataType
            {
                File = 0,
                Directory = 1
            }
            public DataType Type { get; set; }
            public string CompletePath { get; set; }
            public string RelativePath { get; set; }
            public long Size { get; set; }

            public DataInfo(DataType type, string completePath, string relativePath, long size)
            {
                Type = type;
                CompletePath = completePath;
                RelativePath = relativePath;
                Size = size;
            }
        }

        public TransferManager(string dataPath, Host Host)
        {
            DataPath = dataPath;
            this.Host = Host;
            transferWindow = new TransferWindow();
            transferWindow.Title = "Sharing \"" + Path.GetFileName(dataPath) + "\" with " + Host.Name;
            transferWindow.Show();
            transferWindow.Activate();
            transferWindow.Closing += new CancelEventHandler(TransferWindow_Closing);
            totalDataSent = 0;
            totalDataSize = 0;
            dataList = new List<DataInfo>();
            UpdateUI(null);
            transferWindow.CurrentOperationString = "Doing preliminary operations.";

            cancellationTokenSource = new CancellationTokenSource();
            var task = SenderSocketTransferAsync(cancellationTokenSource.Token);
        }

        private void TransferWindow_Closing(object sender, CancelEventArgs e)
        {
            cancellationTokenSource.Cancel();
        }


        private async Task SenderSocketTransferAsync(CancellationToken cancellationToken)
        {
            IPEndPoint remoteEndPoint = new IPEndPoint(Host.EndPoint.Address, Host.EndPoint.Port);
            // Create a TCP/IP socket.
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                // Connect to the remote endpoint.
                cancellationToken.ThrowIfCancellationRequested();
                await socket.ConnectTask(remoteEndPoint).WithCancellation(cancellationToken);
            }
            catch (OperationCanceledException)
            {
                transferWindow.Close();
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                return;
            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Error while connecting to " + Host.Name + "("+ Host.EndPoint.Address + ")", e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error while connecting to " + Host.Name + "(" + Host.EndPoint.Address + "):\n" + e.ToString());
                return;
            }

            try
            {
                //Check whether data still exists
                if(!Directory.Exists(DataPath) && !File.Exists(DataPath))
                {
                    throw new Exception("SenderSocketTransfer - Data to be sent to " + Host.Name + "(" + Host.EndPoint.Address + ") doesn't exist anymore.");
                }

                //Detect whether dataPath is a directory or file
                FileAttributes attr = File.GetAttributes(DataPath);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    //It's a directory
                    long size = ScanDirectory(new DirectoryInfo(DataPath));
                    totalDataSent = 0;
                    totalDataSize = size;
                    UpdateUI(null);

                    //Send a FolderTransferRequest message
                    Message folderTransferRequest = new Message(Message.MessageType.FolderTransferRequest, Manager.UserName, Path.GetFileName(DataPath), size);
                    string folderTransferRequestString = folderTransferRequest.Serialize();
                    byte[] requestData = Encoding.ASCII.GetBytes(folderTransferRequestString);
                    cancellationToken.ThrowIfCancellationRequested();
                    await socket.SendTask(requestData, 0, requestData.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);

                    transferWindow.CurrentOperationString = "Waiting for a response from " + Host.Name + ".";

                    //Wait for a response
                    byte[] dataBuffer = new Byte[1024];
                    cancellationToken.ThrowIfCancellationRequested();
                    int bytesReceived = await socket.ReceiveTask(dataBuffer, 0, dataBuffer.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);
                    string responseString = Encoding.ASCII.GetString(dataBuffer, 0, bytesReceived);
                    if (bytesReceived == 0 || responseString.IndexOf("<EOF>") == -1)
                    {
                        throw new Exception("SenderSocketTransfer - Error while receiving confirmation message from " + Host.Name + "(" + Host.EndPoint.Address + ").");
                    }
                    Console.WriteLine(responseString);
                    Message responseMessage = JsonConvert.DeserializeObject<Message>(responseString, new JsonSerializerSettings { CheckAdditionalContent = false });
                    
                    //Check if the message received is a TransferAgreed Message or a TransferRefused Message
                    if (responseMessage.Type == Message.MessageType.TransferRefused)
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                        transferWindow.CurrentOperationString = "Transfer refused by "+ Host.Name +".";
                        transferWindow.CurrentOperationText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                        transferWindow.CancelButton.Content = "OK";
                        transferWindow.CancelButton.Background = new SolidColorBrush(Color.FromArgb(255, 29, 140, 158));
                        transferWindow.Activate();
                        return;
                    }
                    else if (responseMessage.Type == Message.MessageType.TransferAgreed)
                    {
                        transferWindow.CurrentOperationString = "Transfering data.";

                        startTime = DateTime.Now;
                        var timer = new Timer(UpdateUI, null, 0, 500);

                        foreach (DataInfo data in dataList)
                        {
                            //check if data still exists
                            //if data is a directory and doesn't exist, go to next file
                            if(data.Type == DataInfo.DataType.Directory && !Directory.Exists(data.CompletePath))
                            {
                                continue;
                            }
                            else if(data.Type == DataInfo.DataType.File)
                            {
                                if(!File.Exists(data.CompletePath))
                                {
                                    //If file doesn't exist anymore
                                    //Update size variables
                                    totalDataSize -= data.Size;
                                    UpdateUI(null);
                                    //continue with next file in the list
                                    continue;
                                }
                                else
                                {
                                    //If file exists, check if its size has changed
                                    var f = new FileInfo(data.CompletePath);
                                    if (data.Size != f.Length)
                                    {
                                        //Data size has changed
                                        //Update variables
                                        totalDataSize += (f.Length - data.Size);
                                        data.Size = f.Length;
                                        UpdateUI(null);
                                    }
                                }
                            }

                            cancellationToken.ThrowIfCancellationRequested();
                            await Task.Run(() => SenderSocketSingleDataTransfer(socket, data, cancellationToken));

                        }

                        //Send a FolderTransferEnd Message
                        Message folderTransferEndRequest = new Message(Message.MessageType.FolderTransferEnd);
                        string folderTransferEndRequestString = folderTransferEndRequest.Serialize();
                        requestData = Encoding.ASCII.GetBytes(folderTransferEndRequestString);
                        cancellationToken.ThrowIfCancellationRequested();
                        await socket.SendTask(requestData, 0, requestData.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);

                        timer.Dispose();
                        UpdateUI(null);
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();

                        transferWindow.CurrentOperationString = "Transfer completed.";
                        transferWindow.CurrentOperationText.Foreground = new SolidColorBrush(Color.FromArgb(255, 29,140, 158));

                        transferWindow.CancelButton.Content = "OK";
                        transferWindow.CancelButton.Background = new SolidColorBrush(Color.FromArgb(255, 29, 140, 158));
                        transferWindow.Activate();

                    }
                    else
                    {
                        throw new Exception("SenderSocketTransfer - Message from " + Host.Name + "(" + Host.EndPoint.Address + ") with an unknown message type, expected TransferAgreed or TransferRefused.");
                    }

                }
                else
                {
                    //It's a file
                    long size = new FileInfo(DataPath).Length;
                    DataInfo dataInfo = new DataInfo(DataInfo.DataType.File, DataPath, Path.GetFileName(DataPath), size);
                    totalDataSent = 0;
                    totalDataSize = dataInfo.Size;
                    UpdateUI(null);

                    //Send a FileTransferRequest message
                    Message fileTransferRequest = new Message(Message.MessageType.FileTransferRequest, Manager.UserName, dataInfo.RelativePath, dataInfo.Size);
                    string fileTransferRequestString = fileTransferRequest.Serialize();
                    byte[] requestData = Encoding.ASCII.GetBytes(fileTransferRequestString);
                    cancellationToken.ThrowIfCancellationRequested();
                    await socket.SendTask(requestData, 0, requestData.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);

                    transferWindow.CurrentOperationString = "Waiting for a response from " + Host.Name + ".";

                    //Wait for a response
                    byte[] dataBuffer = new Byte[1024];
                    cancellationToken.ThrowIfCancellationRequested();
                    int bytesReceived = await socket.ReceiveTask(dataBuffer, 0, dataBuffer.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);
                    string responseString = Encoding.ASCII.GetString(dataBuffer, 0, bytesReceived);
                    if (bytesReceived == 0 || responseString.IndexOf("<EOF>") == -1)
                    {
                        throw new Exception("SenderSocketTransfer - Error while receiving confirmation message from " + Host.Name + "(" + Host.EndPoint.Address + ").");
                    }
                    Console.WriteLine(responseString);
                    Message responseMessage = JsonConvert.DeserializeObject<Message>(responseString, new JsonSerializerSettings { CheckAdditionalContent = false });

                    //Check if the message received is a TransferAgreed Message or a TransferRefused Message
                    if (responseMessage.Type == Message.MessageType.TransferRefused)
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                        transferWindow.CurrentOperationString = "Transfer refused by " + Host.Name + ".";
                        transferWindow.CurrentOperationText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                        transferWindow.CancelButton.Content = "OK";
                        transferWindow.CancelButton.Background = new SolidColorBrush(Color.FromArgb(255, 29, 140, 158));
                        transferWindow.Activate();

                        return;
                    }
                    else if(responseMessage.Type == Message.MessageType.TransferAgreed)
                    {
                        if (!File.Exists(dataInfo.CompletePath))
                        {
                            //File doesn't exist anymore
                            throw new Exception("SenderSocketTransfer - File (\"" + dataInfo.RelativePath + "\") to be sent to " + Host.Name + "(" + Host.EndPoint.Address + ") doesn't exist anymore.");
                        }

                        transferWindow.CurrentOperationString = "Transfering data.";

                        startTime = DateTime.Now;
                        var timer = new Timer(UpdateUI, null, 0, 500);
                        cancellationToken.ThrowIfCancellationRequested();
                        await Task.Run(() => SenderSocketSingleDataTransfer(socket, dataInfo, cancellationToken));
                        timer.Dispose();
                        UpdateUI(null);
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();

                        transferWindow.CurrentOperationString = "Transfer completed.";
                        transferWindow.CurrentOperationText.Foreground = new SolidColorBrush(Color.FromArgb(255, 29, 140, 158));

                        transferWindow.CancelButton.Content = "OK";
                        transferWindow.CancelButton.Background = new SolidColorBrush(Color.FromArgb(255, 29, 140, 158));
                        transferWindow.Activate();
                    }
                    else
                    {
                        throw new Exception("SenderSocketTransfer - Message from " + Host.Name + "(" + Host.EndPoint.Address + ") with an unknown message type, expected TransferAgreed or TransferRefused.");
                    }

                }
            }
            catch(OperationCanceledException)
            {
                transferWindow.Close();
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();

            }
            catch (Exception e)
            {
                App.notifyIcon.ShowBalloonTip("Error while transfering data to " + Host.Name, e.Message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Error);
                Console.WriteLine("Error while transfering data to " + Host.Name +":\n" + e.ToString());
                transferWindow.Close();
                if (socket != null)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
            }
        }

        private void UpdateUI(object state)
        {
            transferWindow.Dispatcher.BeginInvoke(new Action(() =>
            {
                transferWindow.ProgressString = Utils.SizeSuffix(totalDataSent, 2) + " / " + Utils.SizeSuffix(totalDataSize, 2);
                if(totalDataSize != 0)
                    transferWindow.ProgressBarStatus.Value = Math.Round((double)totalDataSent / (double)totalDataSize * 100);
                if (totalDataSent != 0)
                    transferWindow.RemainingTimeString = (TimeSpan.FromTicks(DateTime.Now.Subtract(startTime).Ticks * (totalDataSize - totalDataSent) / totalDataSent)).ToString(@"hh\:mm\:ss");
            }));
        }

        private async Task SenderSocketSingleDataTransfer(Socket socket, DataInfo dataInfo, CancellationToken cancellationToken)
        {
            if (dataInfo.Type == DataInfo.DataType.File)
            {
                //Send a FileHeader Message (1024 byte)
                Message fileHeader = new Message(Message.MessageType.FileHeader, dataInfo.RelativePath, dataInfo.Size);
                string fileHeaderString = fileHeader.Serialize();
                byte[] fileHeadertData = new byte[1024];
                fileHeadertData = Encoding.ASCII.GetBytes(fileHeaderString);
                Array.Resize(ref fileHeadertData, 1024);

                cancellationToken.ThrowIfCancellationRequested();
                await socket.SendTask(fileHeadertData, 0, fileHeadertData.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);

                //Send file
                using (FileStream file = File.OpenRead(dataInfo.CompletePath))
                {
                    byte[] sendBuffer = new byte[4096];
                    long bytesLeftToSend = dataInfo.Size;
                    while (bytesLeftToSend > 0)
                    {
                        cancellationToken.ThrowIfCancellationRequested();
                        int bytesRead = await file.ReadAsync(sendBuffer, 0, sendBuffer.Length, cancellationToken);
                        int bytesSent = await socket.SendTask(sendBuffer, 0, bytesRead, SocketFlags.None).WithCancellation<int>(cancellationToken);
                        totalDataSent += bytesSent;
                        bytesLeftToSend -= bytesSent;
                    }
                }
            }
            else if (dataInfo.Type == DataInfo.DataType.Directory)
            {
                //Send a FolderHeader Message (1024 byte)
                Message folderHeader = new Message(Message.MessageType.FolderHeader, dataInfo.RelativePath);
                string folderHeaderString = folderHeader.Serialize();
                byte[] folderHeadertData = new byte[1024];
                folderHeadertData = Encoding.ASCII.GetBytes(folderHeaderString);
                Array.Resize(ref folderHeadertData, 1024);

                cancellationToken.ThrowIfCancellationRequested();
                await socket.SendTask(folderHeadertData, 0, folderHeadertData.Length, SocketFlags.None).WithCancellation<int>(cancellationToken);

            }
        }

        public long ScanDirectory(DirectoryInfo d)
        {
            long size = 0;

            // Scan file in directory d.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                //Create relative path (compared to main directory path)
                Uri completeFilePath = new Uri(fi.FullName);
                Uri mainDirectoryPath = new Uri(DataPath);
                string relativePath = Uri.UnescapeDataString(mainDirectoryPath.MakeRelativeUri(completeFilePath).ToString());
                //Add file to dataList
                dataList.Add(new DataInfo(DataInfo.DataType.File, fi.FullName, relativePath, fi.Length));

                size += fi.Length;
            }

            // Scan subdirectories.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                //Create relative path (compared to main directory path)
                Uri completeDirPath = new Uri(di.FullName);
                Uri mainDirectoryPath = new Uri(DataPath);
                //Uri.UnescapeDataString(mainDirectoryPath.MakeRelativeUri(completeFilePath).ToString().Replace('/', Path.DirectorySeparatorChar));
                string relativePath = Uri.UnescapeDataString(mainDirectoryPath.MakeRelativeUri(completeDirPath).ToString());

                //Add directory to dataList
                dataList.Add(new DataInfo(DataInfo.DataType.Directory, di.FullName, relativePath, 0));

                size += ScanDirectory(di);
            }
            return size;
        }

    }
}
